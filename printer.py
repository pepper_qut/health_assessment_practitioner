import io
import requests
from datetime import datetime

def toHTML(data, pages=None):
  x1 = int(data['walk_sess'])
  y1 = int(data['vig_sess'])
  z1 = int(data['mod_sess'])

  x2 = int(data['walk_time_hrs']) * 60 + int(data['walk_time_mins'])
  y2 = (int(data['vig_time_hrs']) * 60 + int(data['vig_time_mins']))
  z2 = int(data['mod_time_hrs']) * 60 + int(data['mod_time_mins'])

  suffsess = x1 + y1 + z1
  sufftime =  x2 + 2 * y2 + z2

  V = 1 if int(data['serves_vegetables']) >= 5 else 0
  F = 1 if int(data['serves_fruit']) >= 2 else 0
  D = 1 if int(data['serves_dairy']) >= 2 else 0
  P = 1 if int(data['serves_lean_protein']) >= 1 and int(data['serves_lean_protein']) <= 3 else 0
  T = 1 if int(data['takeaway_days']) <= 1 else 0
  Sw = 1 if int(data['treats_days']) <= 1 else 0
  Sa = 1 if int(data['saltyfood_days']) <= 1 else 0

  Dr = 1 if data['dairy_reducedfat'] == 'always' else 0.75 if data['dairy_reducedfat'] == 'mostly' else 0.5 if data['dairy_reducedfat'] == 'sometimes' else 0
  W = 1 if data['wholegrain'] == 'always' else 0.75 if data['wholegrain'] == 'mostly' else 0.5 if data['wholegrain'] == 'sometimes' else 0
  Sdr = 0 if data['sugary_drinks'] == 'always' else 0.5 if data['sugary_drinks'] == 'mostly' else 0.75 if data['sugary_drinks'] == 'sometimes' else 1
  FIntTotal = V + F + D + P + T + Sw + Sa + Dr + W + Sdr

  last_date = datetime.strptime(data['last_use'], '%Y-%m-%d') if int(data['cigarette_screening']) != 0 else 0
  second_last_date = datetime.strptime(data['second_last_use'], '%Y-%m-%d') if int(data['cigarette_screening']) != 0 else 0
  third_last_date = datetime.strptime(data['third_last_use'], '%Y-%m-%d') if int(data['cigarette_screening']) != 0 else 0

  CigW = int(data['last_num_cigarettes']) if int(data['cigarette_screening']) != 0 else 0
  CigX = int(data['second_last_num_cigarettes']) if int(data['cigarette_screening']) != 0 else 0
  CigY = (last_date - second_last_date).days if int(data['cigarette_screening']) != 0 else 0
  CigZ = (second_last_date - third_last_date).days if int(data['cigarette_screening']) != 0 else 0

  CigTotal = (CigW + CigX) / float(CigY + CigZ) if int(data['cigarette_screening']) != 0 else 0

  Alc1 = int(data['alcohol_freq']) if int(data['alcohol_screening']) != 0 else 0
  Alc2 = int(data['alcohol_drinks']) if int(data['alcohol_screening']) != 0 else 0
  Alc3 = int(data['alcohol_binge']) if int(data['alcohol_screening']) != 0 else 0

  positive_lines = []
  negative_lines = []

  items = []
  if V:
    items.append('vegetables')
  if F:
    items.append('fruits')
  if D:
    items.append('dairy')
  if P:
    items.append('protein/poultry')
  if Dr >= 0.75:
    items.append('dairy reduced fat')
  if W >= 0.75:
    items.append('wholegrain cereals')

  if items:
    positive_lines.append('consuming the recommended serves of {}'.format(" and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]))

  items = []
  if T:
    items.append('takeaway')
  if Sw:
    items.append('sweets')
  if Sa:
    items.append('salty foods')

  if items:
    positive_lines.append('eating {} once a week or less'.format(" and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]))

  if Sdr >= 0.75:
    positive_lines.append('not consuming sugary drinks often')

  items = positive_lines
  if items:
    positive_lines = ' You are {}.'.format(", and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0])
  else:
    positive_lines = ''

  items = []
  if V < 1:
    items.append('vegetables')
  if F < 1:
    items.append('fruits')
  if D < 1:
    items.append('dairy')
  if P < 1:
    items.append('protein/poultry')

  if items:
    negative_lines.append('by not eating the recommended serves of {}'.format(" and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]))

  items = []
  if Dr < 0.75:
    items.append('dairy reduced fat')
  if W < 0.75:
    items.append('wholegrain cereals')

  if items:
    negative_lines.append('by not eating mostly {}'.format(" and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]))

  if Sdr < 0.75:
    negative_lines.append('by not limiting sugary drinks')

  items = []
  if T < 1:
    items.append('takeaway')
  if Sw < 1:
    items.append('sweets')
  if Sa < 1:
    items.append('salty foods')

  if items:
    negative_lines.append('by not limiting {} to once a week or less'.format(" and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]))

  items = negative_lines
  if items:
    negative_lines = ' Unfortunately, your eating {} be meeting Australian guidelines {}.'.format(
      'doesn\'t seem to be' if FIntTotal < 4 else 'may not',
      ", and ".join([", ".join(items[:-1]), items[-1]]) if len(items) > 1 else items[0]
    )
  else:
    negative_lines = ''

  F1 = ('It\'s time to improve your eating habits.' + positive_lines + negative_lines).replace('but Unfortunately', 'but unfortunately')
  F2 = ('It\'s good that you are making an effort to eat healthy.' + positive_lines + negative_lines).replace('but Unfortunately', 'but unfortunately')
  F3 = ('Your eating is pretty healthy, ' + ('but' + negative_lines if negative_lines else '') \
     + positive_lines).replace('but Unfortunately', 'but unfortunately')
  
  CigT = 'abstinence' if CigTotal == 0 else \
        'once a week or less' if CigTotal < 0.14 else \
        'more than once a week' if CigTotal < 1 else \
        'daily' if CigTotal < 2 else \
        'more than once a day'

  replacements = {
    'x1': x1,
    'y1': y1,
    'z1': z1,
    'x2': x2,
    'y2': y2,
    'y22': y2 * 2,
    'z2': z2,
    'suffsess': suffsess,
    'sufftime': '1 minute' if sufftime == 1 else '{} minutes'.format(sufftime),
    'total1': ('5+ sessions a week, sufficient' if suffsess >= 5 else '<span class="warning ">&gt;5/week, not sufficient</span>'),
    'total2': ('150+ minutes a week, sufficient' if sufftime  >= 150 else '<span class="warning ">&lt;150min/week, not sufficient</span>'),
    'PAcT': ('sedentary' if sufftime == 0 else ('insufficiently active' if sufftime >= 1 and sufftime <= 149 else ('insufficiently active' if sufftime >= 150 and suffsess < 5 else 'sufficiently active'))),
    'A1': ('Participating in exercise on most days is advised. There is an increased risk of poor health because of inactivity.' if sufftime == 0 else 
          ('It\'s good that you are participating in some activity, but you may need to do more to obtain health benefits.' if sufftime >= 1 and sufftime <= 149 else 
          ('It\'s great that you are making an effort to do some activity, but you may need to do it more often to obtain a significant benefit.' if sufftime >= 150 and suffsess < 5 else 
          'Keep up the good work. This level of activity is likely to help you stay healthy.'))),
    
    'VN': '1 serve/day' if int(data['serves_vegetables']) == 1 else '{} serves/day'.format(int(data['serves_vegetables'])),
    'FN': '1 serve/day' if int(data['serves_fruit']) == 1 else '{} serves/day'.format(int(data['serves_fruit'])),
    'DN': '1 serve/day' if int(data['serves_dairy']) == 1 else '{} serves/day'.format(int(data['serves_dairy'])),
    'PN': '1 serve/day' if int(data['serves_lean_protein']) == 1 else '{} serves/day'.format(int(data['serves_lean_protein'])),
    'TN': '1 serve/week' if int(data['takeaway_days']) == 1 else '{} serves/week'.format(int(data['takeaway_days'])),
    'SwN': '1 serve/week' if int(data['treats_days']) == 1 else '{} serves/week'.format(int(data['treats_days'])),
    'SaN': '1 serve/week' if int(data['saltyfood_days']) == 1 else '{} serves/week'.format(int(data['saltyfood_days'])),
    'DrN': data['dairy_reducedfat'].title(),
    'SdrN': data['sugary_drinks'].title(),
    'WN': data['wholegrain'].title(),

    'V': '<span class="tick">Vegetables = 5+ serves/day</span>' if V else '<span class="warning cross">Vegetables = &lt; 5 serves/day</span>',
    'F': '<span class="tick">Fruit = 2+ serves/day</span>' if F else '<span class="warning cross">Fruit = &lt; 2 serves/day</span>',
    'D': '<span class="tick">Dairy = 2+ serves/day</span>' if D else '<span class="warning cross">Dairy = &lt; 2 serves/day</span>',
    'P': '<span class="tick">Protein/poultry = 1-3 serves/day</span>' if P else '<span class="warning cross">Protein/poultry = {}</span>'.format('&gt;3 serves/day' if int(data['serves_lean_protein']) > 3 else '0 serves/day'),
    'T': '<span class="tick">Takeaway = &lt;= Once/week</span>' if T else '<span class="warning cross">Takeaway = &gt; Once/week</span>',
    'Sw': '<span class="tick">Sweets = &lt;= Once/week</span>' if Sw else '<span class="warning cross">Sweets = &gt; Once/week</span>',
    'Sa': '<span class="tick">Salty foods = &lt;= Once/week</span>' if Sa else '<span class="warning cross">Salty foods = &gt; Once/week</span>',
    'W': '<span class="tick">Wholegrains = {}</span>'.format(data['wholegrain'].title()) if W else '<span class="warning cross">Wholegrains = {}</span>'.format(data['wholegrain'].title()),
    'Dr': '<span class="tick">Dairy reduced fat = {}</span>'.format(data['dairy_reducedfat'].title().replace('\'T', '\'t')) if Dr else '<span class="warning cross">Dairy reduced fat = {}</span>'.format(data['dairy_reducedfat'].title().replace('\'T', '\'t')),
    'Sdr': '<span class="tick">Sugary drinks = {}</span>'.format(data['sugary_drinks'].title()) if Sdr else '<span class="warning cross">Sugary drinks = {}</span>'.format(data['sugary_drinks'].title()),
    'FInT': 'very unhealthy' if FIntTotal == 0 else
            'mostly unhealthy' if FIntTotal >= 1 and FIntTotal < 4 else
            'somewhat healthy' if FIntTotal >= 4 and FIntTotal < 7 else
            'mostly healthy' if FIntTotal >= 7 and FIntTotal < 10 else
            'very healthy',
    'A2': 'Unfortunately, your eating doesn\'t meet Australian guidelines for healthy eating by not consuming the recommended serves of vegetables, fruits, dairy, dairy reduced, wholegrain cereals and protein/poultry, by not reducing your consumption of takeaway, sweets and salty foods to once a week or less, and by not limiting your intake of sugary drinks. Eating healthy by following the guidelines is recommended.' if FIntTotal == 0 else
          F1 if FIntTotal >= 1 and FIntTotal < 4 else
          F2 if FIntTotal >= 4 and FIntTotal < 7 else
          F3 if FIntTotal >= 7 and FIntTotal < 10 else
            'Wow: You are meeting all the Australian guidelines for healthy eating. Keep it up!',
    'CigW': '1 cigarette' if CigW == 1 else '{} cigarettes'.format(CigW),
    'CigX': '1 cigarette' if CigX == 1 else '{} cigarettes'.format(CigX),
    'CigY': '1 day' if CigY == 1 else '{} days'.format(CigY),
    'CigZ': '1 day' if CigZ == 1 else '{} days'.format(CigZ),
    'CigTotal': '<span class="warning">{}, {}</span>'.format(CigTotal if CigTotal < 2 else '2+', CigT) if CigTotal > 0 else '<span class="tick">abstinence</span>',
    'CigT': CigT,
    'A3': 'You\'re wise to not smoke at all. Any cigarette use carries health risks. Keep it up - that\'s great.' if CigTotal == 0 else \
          'Although you are not smoking frequently, refraining from smoking is advised as any consumption carries health risks.' if CigTotal < 0.14 else \
          'Reduced smoking and refraining from smoking is advised as any consumption carries health risks.' if CigTotal < 1 else \
          'Your daily consumption carries health risks. Refraining from smoking and/or seeking a specialist service to help reduce your smoking is advised.' if CigTotal < 2 else \
          'Your high consumption carries significant health risks. Refraining from smoking and/or seeking a specialist service to help reduce your smoking is advised.',
    'Alc1': 'Never' if Alc1 == 0 else \
        'Monthly or less' if Alc1 == 1 else \
        '2-4 times per month' if Alc1 == 2 else \
        '2-3 times per week' if Alc1 == 3 else \
        '4+ times per week',
    'Alc2': '1-2 drinks' if Alc2 == 0 else \
        '3-4 drinks' if Alc2 == 1 else \
        '5-6 drinks' if Alc2 == 2 else \
        '7-9 drinks' if Alc2 == 3 else \
        '10+ drinks',
    'Alc3': 'Never' if Alc3 == 0 else \
        'Less than monthly' if Alc3 == 1 else \
        'Monthly' if Alc3 == 2 else \
        'Weekly' if Alc3 == 3 else \
        'Daily or almost daily',
    'AlcT': 'abstinence' if Alc1 == 0 else \
        'low-risk' if Alc2 + Alc3 == 0 else \
        'moderate-risk' if Alc3 == 0 else \
        'high-risk',
    'AlcTotal': '<span class="tick">Q1 = 0 = Abstinence from drinking</span>' if Alc1 == 0 else \
        '<span class="tick">Q1 > 0 but Q2 + Q3 = 0 = Low-risk drinking</span>' if Alc2 + Alc3 == 0 else \
        '<span class="warning cross">Q1 > 0 and Q2 > 0 but Q3 = 0 = moderate-risk</span>' if Alc3 == 0 else \
        '<span class="warning cross">Q1 > 0 and Q2 > 0 and Q3 > 0 = high-risk drinking</span>',
    'A4': 'You\'re wise to not drink at all. Any alcohol consumption carries some health risks. Keep it up - that\'s great.' if Alc1 == 0 else \
        'You are obviously trying to avoid risky drinking. Any alcohol consumption carries some health risks, but you are not exceeding Australian guidelines for drinking.' if Alc2 + Alc3 == 0 else \
        'Your regular alcohol use is putting you at risk of some health problems in the longer term.' if Alc3 == 0 else \
        'Your drinking is putting you at a significantly increased risk of illness or injury each time you have more than 4 drinks a day.',
    'Section1': 'display:block' if not pages or 1 in pages else 'display: none;',
    'Section2': 'display:block' if not pages or 2 in pages else 'display: none;',
    'Section3': 'display:block' if not pages or 3 in pages else 'display: none;',
    'Section4': 'display:block' if not pages or 4 in pages else 'display: none;',
    'Page1': 'display:block' if not pages or 1 in pages or 2 in pages else 'display: none;',
    'Page2': 'display:block' if not pages or 3 in pages or 4 in pages else 'display: none;',
    'Footer': 'display:block' if not pages else 'display: none;'
  }

  with open('template.html', 'U') as f:
    template = f.read()

    for key in replacements:
      template = template.replace('{' + key + '}', str(replacements[key]))

  return template

def send(html):
  f = io.StringIO(unicode(html, 'UTF-8'))
  files = { 'file': f }

  requests.post('http://cloudvis.qut.edu.au:8082',files=files)

if __name__ == '__main__':
  html = toHTML({
    "age": "30", 
    "alcohol_binge": "0", 
    "alcohol_drinks": "0", 
    "alcohol_freq": "0", 
    "alcohol_robot_likert": "3", 
    "alcohol_screening": "1", 
    "cigarette_screening": "1", 
    "consent": "0", 
    "conversation_human_likert": "2", 
    "conversation_robot_likert": "3", 
    "dairy_reducedfat": "never", 
    "diet_human_likert": "3", 
    "diet_robot_likert": "5", 
    "education": "Postgraduate degree", 
    "exercise_human_likert": "3", 
    "exercise_robot_likert": "4", 
    "exit": "16", 
    "gender": "male", 
    "last_num_cigarettes": "4", 
    "last_use": "2019-04-03", 
    "marital": "In a relationship", 
    "mental_human_likert": "3", 
    "mental_robot_likert": "3", 
    "mod_sess": "0", 
    "mod_time_hrs": "0", 
    "mod_time_mins": "0", 
    "occupation": "Construction", 
    "programming_experience": "10", 
    "robotics_experience": "1", 
    "saltyfood_days": "5", 
    "second_last_num_cigarettes": "6", 
    "second_last_use": "2019-04-02", 
    "serves_dairy": "0", 
    "serves_fruit": "0", 
    "serves_lean_protein": "1", 
    "serves_vegetables": "0", 
    "smoking_human_likert": "3", 
    "smoking_robot_likert": "3", 
    "student": "1", 
    "study": "Finance/Accounting", 
    "sugary_drinks": "always", 
    "takeaway_days": "5", 
    "technology_experience": "9", 
    "third_last_use": "2019-04-01", 
    "time_taken": 359.964, 
    "treats_days": "5", 
    "tutorial": "1", 
    "tutorial_number": "10", 
    "tutorial_slider": "4", 
    "vig_sess": "1", 
    "vig_time_hrs": "0", 
    "vig_time_mins": "23", 
    "walk_sess": "3", 
    "walk_time_hrs": "2", 
    "walk_time_mins": "50", 
    "wholegrain": "never"
  })
  with open('out.html', 'w') as f:
    f.write(html)