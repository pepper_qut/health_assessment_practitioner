<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Practitioner Survey" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="main" src="html/javascripts/main.js" />
        <File name="main.min" src="html/javascripts/main.min.js" />
        <File name="polyfill" src="html/javascripts/polyfill.js" />
        <File name="polyfill.min" src="html/javascripts/polyfill.min.js" />
        <File name="Raleway-Black" src="html/styles/fonts/Raleway-Black.ttf" />
        <File name="Raleway-Regular" src="html/styles/fonts/Raleway-Regular.ttf" />
        <File name="main" src="html/styles/main.css" />
        <File name="service" src="service.py" />
        <File name="launch" src="html/.vscode/launch.json" />
        <File name="main-old" src="html/javascripts/main-old.js" />
        <File name="main-old" src="html/javascripts_original/main-old.js" />
        <File name="main" src="html/javascripts_original/main.js" />
        <File name="main.min" src="html/javascripts_original/main.min.js" />
        <File name="polyfill" src="html/javascripts_original/polyfill.js" />
        <File name="polyfill.min" src="html/javascripts_original/polyfill.min.js" />
        <File name="green_arrow2" src="html/green_arrow2.PNG" />
        <File name="bootstrap.bundle.min" src="html/javascripts/bootstrap.bundle.min.js" />
        <File name="bootstrap.min" src="html/styles/bootstrap.min.css" />
        <File name="template" src="template.html" />
        <File name="printer" src="printer.py" />
        <File name="beer" src="html/images/beer.png" />
        <File name="spirits" src="html/images/spirits.png" />
        <File name="wine" src="html/images/wine.png" />
        <File name="ETH_PepperRobot_Patient_Information-Sheet_05-04-2019_Final" src="html/ETH_PepperRobot_Patient_Information-Sheet_05-04-2019_Final.html" />
        <File name="out" src="out.html" />
        <File name="index" src="html/index.html" />
        <File name="app" src="html/app.html" />
        <File name="ETH_PepperRobot_Practitioner_Information-Sheet_05-04-2019_Final" src="html/ETH_PepperRobot_Practitioner_Information-Sheet_05-04-2019_Final.html" />
        <File name="jquery-ui.min" src="html/javascripts/jquery-ui.min.js" />
        <File name="jquery.min" src="html/javascripts/jquery.min.js" />
        <File name="ui-icons_444444_256x240" src="html/styles/images/ui-icons_444444_256x240.png" />
        <File name="ui-icons_555555_256x240" src="html/styles/images/ui-icons_555555_256x240.png" />
        <File name="ui-icons_777620_256x240" src="html/styles/images/ui-icons_777620_256x240.png" />
        <File name="ui-icons_777777_256x240" src="html/styles/images/ui-icons_777777_256x240.png" />
        <File name="ui-icons_cc0000_256x240" src="html/styles/images/ui-icons_cc0000_256x240.png" />
        <File name="ui-icons_ffffff_256x240" src="html/styles/images/ui-icons_ffffff_256x240.png" />
        <File name="jquery-ui.min" src="html/styles/jquery-ui.min.css" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
    <service name="service" autorun="true" execStart="./python service.py" />
    <executableFiles>
        <file path="python" />
    </executableFiles>
    <qipython name="service" />
</Package>
